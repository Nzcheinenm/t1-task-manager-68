package ru.t1.dkononov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.entity.soap.ProjectFindAllResponse;
import ru.t1.dkononov.tm.entity.soap.TaskClearRequest;
import ru.t1.dkononov.tm.entity.soap.TaskClearResponse;
import ru.t1.dkononov.tm.entity.soap.TaskDeleteByIdRequest;
import ru.t1.dkononov.tm.entity.soap.TaskDeleteByIdResponse;
import ru.t1.dkononov.tm.entity.soap.TaskFindAllRequest;
import ru.t1.dkononov.tm.entity.soap.TaskFindAllResponse;
import ru.t1.dkononov.tm.entity.soap.TaskSaveRequest;
import ru.t1.dkononov.tm.entity.soap.TaskSaveResponse;

@Endpoint
public class TaskSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://dkononov.t1.ru/tm/entity/soap";

    @Autowired
    private TaskDtoService service;

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
           final TaskDeleteByIdResponse response = new TaskDeleteByIdResponse();
           final String id = request.getId();
           service.deleteById(id);
           return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearResponse", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        final TaskClearResponse response = new TaskClearResponse();
        service.clear();
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveResponse", namespace = NAMESPACE)
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        final TaskSaveResponse response = new TaskSaveResponse();
        final TaskDto task = request.getTask();
        service.save(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllResponse", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setId(service.findAll());
        return response;
    }


}
