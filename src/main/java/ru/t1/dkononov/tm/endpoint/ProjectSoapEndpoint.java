package ru.t1.dkononov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.entity.soap.ProjectClearRequest;
import ru.t1.dkononov.tm.entity.soap.ProjectClearResponse;
import ru.t1.dkononov.tm.entity.soap.ProjectDeleteByIdRequest;
import ru.t1.dkononov.tm.entity.soap.ProjectDeleteByIdResponse;
import ru.t1.dkononov.tm.entity.soap.ProjectFindAllRequest;
import ru.t1.dkononov.tm.entity.soap.ProjectFindAllResponse;
import ru.t1.dkononov.tm.entity.soap.ProjectSaveRequest;
import ru.t1.dkononov.tm.entity.soap.ProjectSaveResponse;

@Endpoint
public class ProjectSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://dkononov.t1.ru/tm/entity/soap";

    @Autowired
    private ProjectDtoService service;

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        final ProjectDeleteByIdResponse response = new ProjectDeleteByIdResponse();
        final String id = request.getId();
        service.deleteById(id);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearResponse", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        final ProjectClearResponse response = new ProjectClearResponse();
        service.clear();
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveResponse", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        final ProjectSaveResponse response = new ProjectSaveResponse();
        final ProjectDto Project = request.getProject();
        service.save(Project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllResponse", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setId(service.findAll());
        return response;
    }


}
