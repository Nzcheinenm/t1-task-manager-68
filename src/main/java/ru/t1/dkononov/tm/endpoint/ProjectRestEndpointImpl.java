package ru.t1.dkononov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkononov.tm.api.endpoint.ProjectEndpoint;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements ProjectEndpoint {

    @Autowired
    private ProjectDtoService service;

    @Override
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return service.findAll();
    }

    @Override
    @PostMapping("/save")
    public ProjectDto save(
            @RequestBody ProjectDto project
    ) {
        return service.save(project);
    }

    @Override
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @PathVariable("id") String id
    ) {
        return service.findById(id);
    }

    @Override
    @GetMapping("/exsitsById/{id}")
    public boolean exsistsById(
            @PathVariable("id") String id
    ) {
        return service.exsistsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @PathVariable("id") String id
    ) {
        service.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @RequestBody ProjectDto project
    ) {
        service.delete(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(
            @RequestBody List<ProjectDto> projects
    ) {
        service.deleteAll(projects);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        service.clear();
    }

}
