package ru.t1.dkononov.tm.entity.soap;

import ru.t1.dkononov.tm.entity.dto.ProjectDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id"
})
@XmlRootElement(name = "projectFindAllResponse")
public class ProjectFindAllResponse {

    @XmlElement(required = true)
    protected List<ProjectDto> id;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public List<ProjectDto> getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(List<ProjectDto> value) {
        this.id = value;
    }

}
