package ru.t1.dkononov.tm.entity.soap;


import ru.t1.dkononov.tm.entity.dto.TaskDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taskDto"
})
@XmlRootElement(name = "taskSaveResponse")
public class TaskSaveResponse {

    protected TaskDto task;

    /**
     * Gets the value of the task property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public TaskDto getTask() {
        return task;
    }

    /**
     * Sets the value of the task property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setTask(TaskDto value) {
        this.task = value;
    }

}
