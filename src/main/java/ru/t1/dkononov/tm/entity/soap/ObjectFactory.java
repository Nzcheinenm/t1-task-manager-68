package ru.t1.dkononov.tm.entity.soap;

import ru.t1.dkononov.tm.entity.dto.TaskDto;

import javax.xml.bind.annotation.XmlRegistry;


@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri.purchaseorderschema
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TaskCountResponse }
     * 
     */
    public TaskCountResponse createTaskCountResponse() {
        return new TaskCountResponse();
    }

    /**
     * Create an instance of {@link TaskFindAllRequest }
     * 
     */
    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    /**
     * Create an instance of {@link TaskFindAllResponse }
     * 
     */
    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    /**
     * Create an instance of {@link TaskCountRequest }
     * 
     */
    public TaskCountRequest createTaskCountRequest() {
        return new TaskCountRequest();
    }

    /**
     * Create an instance of {@link TaskClearRequest }
     * 
     */
    public TaskClearRequest createTaskClearRequest() {
        return new TaskClearRequest();
    }

    /**
     * Create an instance of {@link TaskCreateRequest }
     * 
     */
    public TaskCreateRequest createTaskCreateRequest() {
        return new TaskCreateRequest();
    }

    /**
     * Create an instance of {@link TaskDeleteByIdResponse }
     * 
     */
    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    /**
     * Create an instance of {@link TaskDeleteByIdRequest }
     * 
     */
    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    /**
     * Create an instance of {@link TaskSaveRequest }
     * 
     */
    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    /**
     * Create an instance of {@link TaskCreateResponse }
     * 
     */
    public TaskCreateResponse createTaskCreateResponse() {
        return new TaskCreateResponse();
    }

    /**
     * Create an instance of {@link TaskClearResponse }
     * 
     */
    public TaskClearResponse createTaskClearResponse() {
        return new TaskClearResponse();
    }

    /**
     * Create an instance of {@link TaskSaveResponse }
     * 
     */
    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

    /**
     * Create an instance of {@link TaskDto }
     * 
     */
    public TaskDto createTaskDto() {
        return new TaskDto();
    }

}
