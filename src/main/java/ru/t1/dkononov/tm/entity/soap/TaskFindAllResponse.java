package ru.t1.dkononov.tm.entity.soap;

import ru.t1.dkononov.tm.entity.dto.TaskDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id"
})
@XmlRootElement(name = "taskFindAllResponse")
public class TaskFindAllResponse {

    @XmlElement(required = true)
    protected List<TaskDto> id;


    public List<TaskDto> getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(List<TaskDto> value) {
        this.id = value;
    }

}
