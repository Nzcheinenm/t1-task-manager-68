package ru.t1.dkononov.tm.entity.soap;

import ru.t1.dkononov.tm.entity.dto.ProjectDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "project"
})
@XmlRootElement(name = "projectSaveResponse")
public class ProjectSaveResponse {

    protected ProjectDto project;

    /**
     * Gets the value of the project property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public ProjectDto getProject() {
        return project;
    }

    /**
     * Sets the value of the project property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setProject(ProjectDto value) {
        this.project = value;
    }

}
