package ru.t1.dkononov.tm.entity.soap;

import ru.t1.dkononov.tm.entity.dto.ProjectDto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "project"
})
@XmlRootElement(name = "projectFindAllRequest")
public class ProjectFindAllRequest {

}
