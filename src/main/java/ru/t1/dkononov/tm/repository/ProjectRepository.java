package ru.t1.dkononov.tm.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.dkononov.tm.entity.model.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {
}
