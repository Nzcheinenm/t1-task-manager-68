package ru.t1.dkononov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.dkononov.tm.entity.dto.TaskDto;

import java.util.List;

@RequestMapping("/api/tasks")
public interface TaskEndpoint {
    @GetMapping("/findAll")
    List<TaskDto> findAll();

    @PostMapping("/save")
    TaskDto save(
            @RequestBody TaskDto task
    );

    @GetMapping("/findById/{id}")
    TaskDto findById(
            @PathVariable("id") String id
    );

    @GetMapping("/exsitsById/{id}")
    boolean exsistsById(
            @PathVariable("id") String id
    );

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(
            @PathVariable("id") String id
    );

    @PostMapping("/delete")
    void delete(
            @RequestBody TaskDto task
    );

    @PostMapping("/deleteAll")
    void deleteAll(
            @RequestBody List<TaskDto> tasks
    );

    @PostMapping("/clear")
    void clear();
}
