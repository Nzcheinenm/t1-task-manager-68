package ru.t1.dkononov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;

import java.util.List;

@RequestMapping("/api/projects")
public interface ProjectEndpoint {
    @GetMapping("/findAll")
    List<ProjectDto> findAll();

    @PostMapping("/save")
    ProjectDto save(
            @RequestBody ProjectDto project
    );

    @GetMapping("/findById/{id}")
    ProjectDto findById(
            @PathVariable("id") String id
    );

    @GetMapping("/exsitsById/{id}")
    boolean exsistsById(
            @PathVariable("id") String id
    );

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(
            @PathVariable("id") String id
    );

    @PostMapping("/delete")
    void delete(
            @RequestBody ProjectDto project
    );

    @PostMapping("/deleteAll")
    void deleteAll(
            @RequestBody List<ProjectDto> projects
    );

    @PostMapping("/clear")
    void clear();
}
